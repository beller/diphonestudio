Diphone Studio is a group of several applications.
Its main function is to provide a powerful control of sound synthesis.
Several analysis / synthesis models are accessible. They are integrated into Diphone Studio in the form
of external programmes called models or plugins. This is the list of them:
• The analysis and additive synthesis model called Additive;
• The resonance model used for analysis and synthesis using formantic wave functions (FOF). This
model is called Chant;
• A last model used for easier processing of external sound files, called Direct Signal.
The central application, Diphone, communicates with the analysis and synthesis programmes, Addan
and Resan, and uses their data to create interpolations.
This complex physical data become accessible in the form of graphical representations (Bpf curves,
slotted "tiles"...).
The user manipulates these elements as he wishes to finally obtain a musical sequence that is saved
onto the hard disk of the computer.
This manual corresponds to versions:
• 2.8 of the Diphone application
• 1.8 of the Addan application
• 1.0 of the Resan application (first public delivery of this application).
It is laid out in the following way.
• Analysis using the additive model: the Addan programme.
• Presentation of the Diphone program:
- Creation of scripts, dictionnaries and sequences
- The Direct Signal model
- The Chant model.
• The Resan program.
• Menus and preferences.
In Diphone, whichever the model used, the vast majority of manipulations remain similar for the user. In
this manual, a detailed review of the functions of the programme is made for the Additive model. Points
specific to the Direct Signal and Chant models are exposed, and cross-references are given for the
general manipulations.